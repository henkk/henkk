
test:
	@echo "Setup NGrok Tunnel. Public Endpoint is: $(shell curl localhost:4040/api/tunnels | jq .tunnels[0].public_url -r)"


up: require-root
	@docker-compose -f dev-experience/docker-compose.yml -p henkk up -d
	@dev-experience/await-ngrok-tunnel.sh
#	@while  "$(shell curl localhost:4040/api/tunnels 2>/dev/null | jq .tunnels[0] -r )" = "null"; do sleep 0.1;  done
#	@echo "Setup NGrok Tunnel. Public Endpoint is: $(shell curl localhost:4040/api/tunnels | jq .tunnels[0].public_url -r)"

down: require-root
	@docker-compose -f dev-experience/docker-compose.yml -p henkk stop

reset-devenv: require-root
	@docker-compose -f dev-experience/docker-compose.yml -p henkk down -v
require-root:
	@if [ "$(shell dev-experience/checkRoot.sh)" = "true" ];then echo "\033[0;31mYou are not root, run this target as root please\033[0m"; exit 1; fi
