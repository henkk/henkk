package de.newcron.henkk.commands.ci;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService;
import de.newcron.henkk.chat.api.ci.ReleaseService;
import de.newcron.henkk.chat.api.ci.VersionControlService;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.ManagedProjectRepository;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@CommandDescription(pattern = "release ${projectName} at ${revision}", group = "Continuous Integration")
@RequiredArgsConstructor
public class ReleaseCommand implements Command {

    private final ReleaseService releaseService;

    private final ContinuousIntegrationService ciService;

    private final ManagedProjectRepository projectService;

    private final VersionControlService versionControlService;

    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        var projectName = message.getVariables().get("projectName");
        var revision = new Sha(message.getVariables().get("revision"));
        var project = projectService.findByProjectName(projectName);
        if (project.isEmpty()) {
            respond.withText("Sorry, but  I don't know a project with that name.");
            return;
        }
        var change = versionControlService.getChangeForCommit(project.get(), revision);
        if (!change.isReadyForDeployment()) {
            respond.withText("Sorry, can't release because the pipeline has failed or the release job can't be triggered for that pipeline");
            return;
        }


        respond.withText("Alright, starting release");
        releaseService.release(project.get(), change, message.getSender().getUniqueUserIdentifier(), ((state, msg) -> {
            respond.withText(switch (state) {
                case SUCCESS -> "Release Successful";
                case FAILURE -> "Release Failed! " + msg;
                case ABORTED -> "Release was aborted";
                case RUNNING -> throw new IllegalStateException("release finished callback was called but release is still running");
            });
        }));

    }
}
