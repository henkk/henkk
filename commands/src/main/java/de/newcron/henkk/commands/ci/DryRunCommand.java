package de.newcron.henkk.commands.ci;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.ci.VersionControlService;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.outbound.richresponse.DryRunResponse;
import de.newcron.henkk.chat.api.outbound.richresponse.DryRunResponse.Commit;
import de.newcron.henkk.chat.api.project.ManagedProjectRepository;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@CommandDescription(pattern = "dry run ${projectName} at ${revision}", description = "show list of changes that would be deployed for the given revision", group = "Continuous Integration")
@RequiredArgsConstructor
public class DryRunCommand implements Command {

    private final ManagedProjectRepository projectService;

    private final VersionControlService versionControlService;

    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        var project = projectService.findByProjectName(message.getVariables().get("projectName"));
        if (project.isEmpty()) {
            respond.withText("Sorry, could not find the project.");
            return;
        }
        if (project.get().getCurrentRelease().isEmpty()) {
            respond.withText("Sorry, I don't know what version is on production right now.");
            return;
        }


        Sha targetRevision = new Sha(message.getVariables().get("revision"));
        var diff = versionControlService.getDiff(project.get(), project.get().getCurrentRelease().get().getSha(), targetRevision);

        var response= DryRunResponse.builder()
            .projectName(project.get().getName())
            .currentlyDeployedRevision(project.get().getCurrentRelease().get().getSha())
            .targetRevision(targetRevision)
            .commits(diff.getPreviousCommits().stream().map(c -> Commit.builder().commitMessage(c.getMessage()).author(c.getCommitter().asString()).revision(c.getSha()).build()).toList())
            .build();
        respond.with(response);

    }


}
