package de.newcron.henkk.commands.projectmanagement;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.ExternalProjectIdentifier;
import de.newcron.henkk.chat.api.project.ManagedProjectService;
import de.newcron.henkk.chat.api.project.ManagedProjectService.ProjectAlreadyImportedException;
import de.newcron.henkk.chat.api.project.ManagedProjectService.ProjectNameAlreadyTakenException;
import de.newcron.henkk.chat.api.project.ProjectDetailsLoader;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@CommandDescription(pattern = "import project ${identifier} as ${projectName}", group = "Project Management")
@RequiredArgsConstructor
public class ImportProjectCommand implements Command {

    private final ProjectDetailsLoader projectDetailsLoader;
    private final ManagedProjectService projectService;

    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        String projectIdentifier = message.getVariables().get("identifier");

        final var searchResult = projectDetailsLoader.load(new ExternalProjectIdentifier(projectIdentifier));
        if(searchResult.isEmpty()) {
            respond.withText("I am sorry, but I could not find the project you were looking for");
            return;
        }
        final var project = searchResult.get();

        try {
            projectService.manage(project, message.getVariables().get("projectName"));
            respond.withText(String.format("Imported Project %s from %s", project.getName(), project.getUrl()));
        } catch (ProjectAlreadyImportedException e) {
            respond.withText("Can't import the project, that repository is already managed");
        } catch (ProjectNameAlreadyTakenException e) {
            respond.withText("Can't import the project, the name is already taken.");
        }


    }
}
