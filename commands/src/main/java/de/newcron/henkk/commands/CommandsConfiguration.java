package de.newcron.henkk.commands;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;


@ComponentScan(basePackageClasses = CommandsConfiguration.class)
@org.springframework.context.annotation.Configuration
@Component
public class CommandsConfiguration {
}
