package de.newcron.henkk.commands.projectmanagement;


import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.OutboundMessage;

import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.ManagedProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@CommandDescription(pattern = "list projects", group = "Project Management")
@RequiredArgsConstructor
public class ListProjectCommand implements Command {

    private final ManagedProjectRepository managedProjectRepository;

    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        var items = managedProjectRepository.findAll().stream().map(x->x.getName()).collect(Collectors.joining("\n"));
        respond.withText("Found Projects:\n"+items);
    }
}
