package de.newcron.henkk.chat.project.gitlab;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
public class GitlabHenkkConfig {
    private  String deployJobName;
}
