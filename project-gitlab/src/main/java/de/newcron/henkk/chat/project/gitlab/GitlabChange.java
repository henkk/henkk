package de.newcron.henkk.chat.project.gitlab;

import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.ci.Commit;
import lombok.Data;
import org.gitlab4j.api.models.Job;

import java.nio.channels.Pipe;
import java.util.Optional;

@Data
public class GitlabChange implements Change {
    private final Commit commit;
    private final boolean isReadyForDeployment;
    private final PipelineState pipelineState;
    private final org.gitlab4j.api.models.Commit gitlabApiCommit;
    private final Optional<Job> releaseTriggerJob;
}
