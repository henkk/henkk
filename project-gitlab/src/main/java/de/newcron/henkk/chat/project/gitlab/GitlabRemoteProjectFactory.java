package de.newcron.henkk.chat.project.gitlab;

import org.gitlab4j.api.GitLabApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ConditionalOnProperty(name = "henkk.project", havingValue = "gitlab")
@ComponentScan(basePackageClasses = GitlabRemoteProjectFactory.class)
class GitlabRemoteProjectFactory  {

    @Bean
    public GitLabApi gitlabApi(@Value("${henkk.project.gitlab.baseurl}") String baseUrl, @Value("${henkk.project.gitlab.personalaccesstoken}") String accessToken) {
        return new GitLabApi(baseUrl, accessToken);
    }
}
