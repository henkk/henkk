package de.newcron.henkk.chat.project.gitlab;

import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.*;
import de.newcron.henkk.chat.api.ci.Change.PipelineState;
import de.newcron.henkk.chat.api.project.ManagedProject;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.CommitRef.RefType;
import org.gitlab4j.api.models.Job;
import org.gitlab4j.api.models.JobStatus;
import org.gitlab4j.api.models.PipelineStatus;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.time.ZoneId;
import java.util.Optional;

import static de.newcron.henkk.chat.api.ci.Commit.builder;

@Component
@RequiredArgsConstructor
public class GitlabVersionControlService implements VersionControlService {

    private final GitLabApi client;


    @Override
    public Change getChangeForCommit(@NonNull ManagedProject project, @NonNull Sha sha) {

        try {
            var projectId = project.getExternalIdentifier().asString();
            var defaultBranchName = client.getProjectApi().getProject(projectId).getDefaultBranch();
            var commit = client.getCommitsApi().getCommit(projectId, sha.asString());
            var refs = client.getCommitsApi().getCommitRefs(projectId, sha.asString());
            var jobs = client.getJobApi().getJobsForPipeline(projectId, commit.getLastPipeline().getId());

            var canTrigger = false;
            Optional<Job> releaseTriggerJob = Optional.empty();
            if (commit.getLastPipeline().getStatus() == PipelineStatus.SUCCESS) {
                var henkkConfigFile = client.getRepositoryFileApi().getFile(projectId, ".henkk", defaultBranchName, true);
                var descriptor = new Yaml().loadAs(henkkConfigFile.getDecodedContentAsString(), GitlabHenkkConfig.class);

                releaseTriggerJob = jobs.stream().filter(j -> j.getName().equalsIgnoreCase(descriptor.getDeployJobName())).findFirst();

                canTrigger = releaseTriggerJob.isPresent() && releaseTriggerJob.get().getStatus() == JobStatus.MANUAL;
            }


            var isOnDefaultBranch = refs.stream().anyMatch(x -> x.getType() == RefType.BRANCH && x.getName().equals(defaultBranchName));

            return new GitlabChange(mapCommit(commit, isOnDefaultBranch),
                canTrigger,
                mapPipelineStatus(commit.getLastPipeline().getStatus()),
                commit,
                releaseTriggerJob
            );

        } catch (GitLabApiException e) {
            throw new IllegalStateException("Could not load details for " + project.getName() + " and commmit " + sha.asString(), e);
        }

    }

    private Commit mapCommit(org.gitlab4j.api.models.Commit commit, boolean isOnDefaultBranch) {
        return builder()
            .sha(new Sha(commit.getShortId()))
            .committer(new UserIdentifier(commit.getAuthorEmail()))
            .onDefaultBranch(isOnDefaultBranch)
            .message(commit.getMessage())
            .timestamp(commit.getCreatedAt().toInstant().atZone(ZoneId.of("UTC")))
            .build();
    }

    @Override
    public ChangeDiff getDiff(ManagedProject managedProject, Sha from, Sha to) {
        var changeToTarget = getChangeForCommit(managedProject, to);

        try {
            var diffs = client.getRepositoryApi().compare(managedProject.getId().getClass(), from.asString(), to.asString(), true);
            return new ChangeDiff(changeToTarget, diffs.getCommits().stream().map(c->mapCommit(c, changeToTarget.getCommit().isOnDefaultBranch())).toList());
        } catch (GitLabApiException e) {
            throw new IllegalStateException("could not load diff for " + managedProject.getName()+": "+from.asString()+"..."+to.asString());
        }
    }


    private PipelineState mapPipelineStatus(@NonNull PipelineStatus status) {
        return switch (status) {
            case CANCELED -> PipelineState.ABORTED;
            case CREATED -> PipelineState.PENDING;
            case PENDING -> PipelineState.PENDING;
            case FAILED -> PipelineState.RED;
            case MANUAL -> PipelineState.PENDING;
            case SCHEDULED -> PipelineState.PENDING;
            case SKIPPED -> PipelineState.ABORTED;
            case RUNNING -> PipelineState.RUNNING;
            case PREPARING -> PipelineState.PENDING;
            case SUCCESS -> PipelineState.GREEN;
            case WAITING_FOR_RESOURCE -> PipelineState.PENDING;
        };
    }
}
