package de.newcron.henkk.chat.project.gitlab;

import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.ci.Change.PipelineState;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release.ReleaseState;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.PipelineStatus;
import org.glassfish.jersey.internal.guava.Maps;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Component
@Log4j2
class GitlabCiService implements ContinuousIntegrationService {

    @NonNull
    private final GitLabApi apiClient;

    private final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(5, new ThreadFactoryBuilder().setDaemon(true).setNameFormat("gitlab-job-poller-%d").build());

    @Override
    public void release(Project project, Change changeOrig) {
        var change = (GitlabChange) changeOrig;
        try {
            apiClient.getJobApi().playJob(project.getExternalIdentifier().asString(), change.getReleaseTriggerJob().orElseThrow().getId());
        } catch (GitLabApiException e) {
            throw new IllegalStateException("Could not start Job for Release", e);
        }

    }

    @Override
    public void awaitUpdates(Project project, Change change, Duration timeout, UpdateListener updateListener) {
        var until = LocalDateTime.now().plus(timeout);
        scheduledExecutorService.schedule(
            new Poller(project, (GitlabChange) change, until, updateListener),
            1, TimeUnit.SECONDS);
        // pipeline status changes back to "running" for while the job runs
        // then it gets back to success or error - depending on the job outcome
    }

    @RequiredArgsConstructor
    class Poller implements Runnable {

        private final Project project;
        private final GitlabChange change;
        private final LocalDateTime until;
        private final UpdateListener updateListener;

        private static final Map<PipelineStatus, ReleaseState> TERMINAL_STATES;

        static {
            var m = new HashMap<PipelineStatus, ReleaseState>();
            m.put(PipelineStatus.CANCELED, ReleaseState.ABORTED);
            m.put(PipelineStatus.FAILED, ReleaseState.FAILURE);
            m.put(PipelineStatus.SUCCESS, ReleaseState.SUCCESS);

            TERMINAL_STATES = Collections.unmodifiableMap(m);
        }

        @Override
        public void run() {
            try {
                var pipeline = apiClient.getPipelineApi().getPipeline(project.getExternalIdentifier().asString(), change.getGitlabApiCommit().getLastPipeline().getId());
                if(TERMINAL_STATES.containsKey(pipeline.getStatus())) {
                    updateListener.onUpdate(TERMINAL_STATES.get(pipeline.getStatus()), "");
                    return;
                }
                if(LocalDateTime.now().isBefore(until)) {
                    scheduledExecutorService.schedule(this, 5, TimeUnit.SECONDS);
                }

            } catch (Exception e) {
                log.error("Could not poll pipeline information", e);
            }

        }
    }
}
