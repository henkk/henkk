package de.newcron.henkk.chat.project.gitlab;


import de.newcron.henkk.chat.api.project.ExternalProjectIdentifier;
import de.newcron.henkk.chat.api.project.ProjectDescriptor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Optional;

@Component
@RequiredArgsConstructor
class ProjectDetailsLoader implements de.newcron.henkk.chat.api.project.ProjectDetailsLoader {

    @NonNull
    private final GitLabApi apiClient;

    @Override
    public Optional<ProjectDescriptor> load(ExternalProjectIdentifier externalProjectIdentifier) {
        try {



            final var details = apiClient.getProjectApi().getProject(externalProjectIdentifier.asString());
            return  Optional.of(new GitlabProjectDescriptor(details));
        } catch (GitLabApiException e) {
            if(e.getHttpStatus() == 404) {
                return Optional.empty();
            }
            throw new IllegalStateException("Could not load Project "+externalProjectIdentifier.asString()+": issue is "+e.getMessage(), e);
        }

    }

    @RequiredArgsConstructor
    static class GitlabProjectDescriptor implements ProjectDescriptor {
        @Getter
        private final Project details;

        @Override
        public @NonNull String getName() {
            return details.getName();
        }

        @SneakyThrows
        @Override
        public @NonNull URI getUrl() {
            return new URI(details.getWebUrl());
        }

        @Override
        public @NonNull ExternalProjectIdentifier getExternalId() {
            return new ExternalProjectIdentifier(String.valueOf(details.getId()));
        }


    }
}
