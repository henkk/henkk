#!/bin/bash

if [[ $OSTYPE == 'darwin'* ]]; then
    echo false
    exit
fi

if grep -qEi "(Microsoft|WSL)" /proc/version &> /dev/null ; then
    echo false
    exit
fi

echo true
