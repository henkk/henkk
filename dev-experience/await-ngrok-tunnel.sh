#!/bin/bash

getTunnel () {
	curl localhost:4040/api/tunnels 2>/dev/null  | jq ".tunnels[] | select(.name == \"command_line\") | .public_url" -r
}

getTarget() {
        curl localhost:4040/api/tunnels 2>/dev/null  | jq ".tunnels[] | select(.name == \"command_line\") | .config.addr" -r
}


X=$(getTunnel)
while  [ "$X" = "null" ]; do

    sleep 0.1
    X=$(getTunnel)
done

echo "Ngrok Tunnel is set up. Public URL is $X, pointing to $(getTarget)"
