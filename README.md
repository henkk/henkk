# Henkk - the g33ky Continuous Delivery Bot

Meet Henkk. Henkk is a robot. He's a fan of Doctor Who, Star Trek and Role Playing Games. Professionally, he believs in software craftsmanship. And he's here to help you with your Continuous Delivery. 

This is what Henkk can do:  

* Keeps track of all your Projects
* Stays up to date with CI pipeline states
* Allows you to deploy from Slack/Teams/&hellip;   
* Create meaningful change history based on commit messages and ticket references

In short, Henkk brings together information from all the places a software engineer touches. 

## Configuration
Configuring Henkk is certainly not as complex as recalibrating the warp matrix onboard the USS Enterprise, but it's certainly harder than configuring a Wordpress instance.

Henkk's configuration has two levels: 

1. System Configuration: is where you configure what external systems he should use (e.g. should he connect to github or gitlab repositories)
2. Component Configuration: is where each of the external systems are configured (e.g. if you want to use github as your VCS system, that will require github specific settings)

All these configurations are specified via java command line arguments such as `--henkk.chat=stdin`
When assembling the configuration for your needs, please make a choice for each of the system configuration bits. For the elements you've selected see below if they require additional configuration and add that one as well. 


#### System Configuration

* `henkk.chat`: Select to which chat system Henkk should connect: 
    * `slack`: connect to Slack
    * `msteams`: connect to MS Teams
    * `stdio`: henkk will talk just to you via the console (for testing or development purposes)

* `henkk.project`: Version Control System where your code is hosted
    * `gitlab`: on gitlab
    * `github`: on github
    
#### Slack Configuration

#### MS Teams Configuration

#### Gitlab Configuration
```
--henkk.project.gitlab.personalaccesstoken=A_GITLAB_PERSONAL_ACCESS_TOKEN \
--henkk.project.gitlab.baseurl=BASE_URL_TO_YOUR_GITLAB_INSTALLATION
```

#### Github Configuration



