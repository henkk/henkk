package de.newcron.henkk.commands.ci;

import com.google.common.collect.ImmutableMap;
import de.newcron.henkk.chat.api.ChatIdentity;
import de.newcron.henkk.chat.api.DisplayName;
import de.newcron.henkk.chat.api.Message;
import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService.UpdateListener;
import de.newcron.henkk.chat.api.ci.ReleaseService;
import de.newcron.henkk.chat.api.ci.VersionControlService;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.ManagedProject;
import de.newcron.henkk.chat.api.project.ManagedProjectRepository;
import de.newcron.henkk.chat.api.project.Sha;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ReleaseCommandTest {

    @Mock
    private ReleaseService releaseService;

    @Mock
    private ManagedProject project;

    @Mock
    private ManagedProjectRepository managedProjectService;

    @Mock
    private VersionControlService versionControlService;

    @Mock
    private ResponseOptions r;

    @Mock
    private UpdateListener
    updateListener;

    @Mock
    private Change change;

    @InjectMocks
    private ReleaseCommand releaseCommand;

    private ReceivedMessage m = new ReceivedMessage(Message.builder().message("").build(), ImmutableMap.of("projectName", "project", "revision", "rev"), new ChatIdentity(new DisplayName("foo"), new UserIdentifier("bar")));


    @BeforeEach
    void setUp() {

    }

    @Test
    void failsWhenProjectUnknown() {
        Mockito.when(managedProjectService.findByProjectName("project")).thenReturn(Optional.empty());
        releaseCommand.handle(m, r);

        Mockito.verifyNoInteractions(releaseService);
    }

    @Test
    void startsReleaseWhenProjectKnownAndReadyForRelease() {
        Mockito.when(managedProjectService.findByProjectName("project")).thenReturn(Optional.of(project));
        Mockito.when(versionControlService.getChangeForCommit(any(), any())).thenReturn(change);
        Mockito.when(change.isReadyForDeployment()).thenReturn(true);

        releaseCommand.handle(m, r);

        verify(releaseService).release(
            eq(project), eq(change), eq(new UserIdentifier("bar")), any());
    }

    @Test
    void doesNotStartProjectWhenNotReadyForRelease() {
        Mockito.when(managedProjectService.findByProjectName("project")).thenReturn(Optional.of(project));
        Mockito.when(versionControlService.getChangeForCommit(any(), any())).thenReturn(change);


        releaseCommand.handle(m, r);

        Mockito.verifyNoInteractions(releaseService);
    }
}
