package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CommandsTest {

    @Test
    void catchAllAddedToCommandsListEnd() {
        List<Class> items = new Commands(singletonList(new DummyCommand())).getCommandList().stream().map(c -> c.getCommand().getClass()).collect(Collectors.toList());

        assertThat(items).containsExactly(DummyCommand.class, CatchAllCommand.class);
    }

    @Test
    void commandsAreSortedByPriority() {
        List<Class> items = new Commands(Arrays.asList(new DummyCommand(), new DummyCommand2())).getCommandList().stream().map(c -> c.getCommand().getClass()).collect(Collectors.toList());

        assertThat(items).containsExactly(DummyCommand2.class, DummyCommand.class, CatchAllCommand.class);
    }


    @CommandDescription(pattern = "dummy")
    class DummyCommand implements Command {

        @Override
        public void handle(ReceivedMessage message, ResponseOptions respond) {

        }
    }

    @CommandDescription(pattern = "dummy command")
    class DummyCommand2 implements Command {

        @Override
        public void handle(ReceivedMessage message, ResponseOptions respond) {

        }
    }

}
