package de.newcron.henkk.application.chat.routing;

import com.google.common.collect.ImmutableList;
import de.newcron.henkk.chat.api.Message;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TokenSequenceTest {

    @Test
    void splitsTokensCorrectly() {
        assertThat(TokenSequence.fromMessage(Message.builder().message("hello world").build())).isEqualTo(new TokenSequence(ImmutableList.of("hello", "world")));
    }

    @Test
    void splitsMultipleSpacesCorrectly() {
        assertThat(TokenSequence.fromMessage(Message.builder().message("hello     world").build())).isEqualTo(new TokenSequence(ImmutableList.of("hello", "world")));
    }

    @Test
    void splitsEntercorrectly() {
        assertThat(TokenSequence.fromMessage(Message.builder().message("hello\nworld").build())).isEqualTo(new TokenSequence(ImmutableList.of("hello", "world")));
    }
}
