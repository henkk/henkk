package de.newcron.henkk.application.ci;

import de.newcron.henkk.application.project.persistence.PersistedRelease;
import de.newcron.henkk.application.project.persistence.ReleaseRepository;
import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService.UpdateListener;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release;
import de.newcron.henkk.chat.api.project.Release.ReleaseState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.http.HttpResponse.ResponseInfo;
import java.time.Duration;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReleaseServiceImplTest {

    @Mock
    private PersistedReleaseService persistedReleaseService;

    @Mock
    private ContinuousIntegrationService ciService;


    @InjectMocks
    private ReleaseServiceImpl r;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Project p;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Change c;

    UserIdentifier u = new UserIdentifier("foouser");

    @Mock
    PersistedRelease rel;


    @Captor
    ArgumentCaptor<PersistedRelease> releaseCaptor;


    @BeforeEach
    void setUp() {
        when(persistedReleaseService.createRelease(p, c, u)).thenReturn(rel);

        Mockito.lenient().doAnswer(invocation -> {
            ((UpdateListener)invocation.getArgument(3)).onUpdate(ReleaseState.SUCCESS, "all good");
            return null;
        }).when(ciService).awaitUpdates(eq(p), eq(c), any(), any());
    }

    @Test
    void persistsReleaseWhenCAlled() {
        r.release(p, c, u, this::nothing);

        verify(persistedReleaseService).createRelease(p, c, u);

    }



    @Test
    void marksReleaseAsFailedWhenReleaseThrowsException() {
        doThrow(new IllegalStateException()).when(ciService).release(p, c);

        r.release(p, c, u, this::nothing);

        verify(persistedReleaseService).failRelease(eq(rel), anyString());
    }

    @Test
    void triggersCiRelease() {
        r.release(p, c, u, this::nothing);
        verify(ciService).release(p, c);
    }

    @Test
    void updatesReleaseWithUpdateEvent() {
        r.release(p, c, u, this::nothing);

        Mockito.verify(persistedReleaseService).persistReleaseOutcome(rel, ReleaseState.SUCCESS, "all good");
    }




    private void nothing(ReleaseState releaseState, String s) {
    }
}
