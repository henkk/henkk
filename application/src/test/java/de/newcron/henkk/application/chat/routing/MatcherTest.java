package de.newcron.henkk.application.chat.routing;

import org.junit.jupiter.api.Test;

import static de.newcron.henkk.application.chat.routing.TokenSequence.fromMessage;
import static de.newcron.henkk.application.chat.routing.TokenSequencePattern.fromPatternSyntax;
import static org.assertj.core.api.Assertions.assertThat;

class MatcherTest {

    private final TokenSequencePattern helloWorldPattern = fromPatternSyntax("hello world");

    @Test
    void matchesSamePattern() {
        assertThat(helloWorldPattern.matches(fromMessage("hello world")).isMatch()).isTrue();
    }

    @Test
    void matchesSamePatternInTheMiddle() {
        assertThat(helloWorldPattern.matches(fromMessage("foo hello world foo")).isMatch()).isTrue();
    }

    @Test
    void matchesSamePatternInTheBeginning() {
        assertThat(helloWorldPattern.matches(fromMessage("hello world foo")).isMatch()).isTrue();
    }

    @Test
    void matchesSamePatternInTheEnd() {
        assertThat(helloWorldPattern.matches(fromMessage("foo hello world")).isMatch()).isTrue();
    }

    @Test
    void resetsAndRestartsPattern() {
        assertThat(helloWorldPattern.matches(fromMessage("hello foo hello world")).isMatch()).isTrue();
    }

    @Test
    void doesNotMatchPartOfPattern() {
        assertThat(helloWorldPattern.matches(fromMessage("hello moon")).isMatch()).isFalse();
    }

    @Test
    void doesNotMatchOnlySubpart() {
        assertThat(helloWorldPattern.matches(fromMessage("hello")).isMatch()).isFalse();
    }

    @Test
    void matchesWithVariables() {
        assertThat(fromPatternSyntax("hello ${planet}").matches(fromMessage("hello mars")).isMatch()).isTrue();
    }

    @Test
    void extractsVariables() {
        Result result = fromPatternSyntax("hello ${planet}").matches(fromMessage("hello mars"));
        assertThat(result.getVariables().get("planet")).isEqualTo("mars");
    }

    @Test
    void matchesEmptyContents() {
        assertThat(fromPatternSyntax("").matches(fromMessage("")).isMatch()).isTrue();
    }

    @Test
    void matchesEmptyContentsWhitespace() {
        assertThat(fromPatternSyntax("").matches(fromMessage(" ")).isMatch()).isTrue();
    }


    @Test
    void supportsLongVariablesAtTheEnd() {
        Result result = fromPatternSyntax("hello ${planets..}").matches(fromMessage("hello mars, venus and earth"));
        assertThat(result.isMatch()).isTrue();
    }

    @Test
    void supportsLongVariablesInTheMiddle() {
        Result result = fromPatternSyntax("hello ${planets..} how are you?").matches(fromMessage("hello mars, venus and earth how are you?"));
        assertThat(result.isMatch()).isTrue();

    }
    @Test
    void extractsLongVariablesInTheMiddle() {
        Result result = fromPatternSyntax("hello ${planets..} how are you?").matches(fromMessage("hello mars, venus and earth how are you?"));
        assertThat(result.getVariables().get("planets")).isEqualTo("mars, venus and earth");

    }

    @Test
    void matchesLongVariablesNonGreedy() {
        Result result = fromPatternSyntax("hello ${planets..} and earth").matches(fromMessage("hello mars, venus and earth"));
        assertThat(result.getVariables().get("planets")).isEqualTo("mars, venus");

    }

    @Test
    void extractsLongVariablesAtTheEnd() {
        Result result = fromPatternSyntax("hello ${planets..}").matches(fromMessage("hello mars, venus and earth"));
        assertThat(result.getVariables().get("planets")).isEqualTo("mars, venus and earth");
    }

    @Test
    void longVariablesNeedAtLeastOneElement() {
        Result result = fromPatternSyntax("hello ${planets..}").matches(fromMessage("hello"));
        assertThat(result.isMatch()).isFalse();
    }


}
