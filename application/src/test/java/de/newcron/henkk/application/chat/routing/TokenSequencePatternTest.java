package de.newcron.henkk.application.chat.routing;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class TokenSequencePatternTest {
    @Test
    void detectsNormalWords() {
        assertThat(new TokenSequencePattern(Arrays.asList(new Word("foo"), new Word("bar")))).isEqualTo(TokenSequencePattern.fromPatternSyntax("foo bar"));
    }

    @Test
    void detectsSimpleVariable() {
        assertThat(new TokenSequencePattern(Arrays.asList(new Word("foo"), new Variable("bar", false)))).isEqualTo(TokenSequencePattern.fromPatternSyntax("foo ${bar}"));
    }

    @Test
    void detectsLongVariable() {
        assertThat(new TokenSequencePattern(Arrays.asList(new Word("foo"), new Variable("bar", true)))).isEqualTo(TokenSequencePattern.fromPatternSyntax("foo ${bar..}"));
    }
}
