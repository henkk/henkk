package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.chat.api.Command;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
// import org.mockito.junit.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CommandImplementationPriorityTest {


    @Mock
    Command cA, cB;


    CommandImplementation commandA, commandB;



    @BeforeEach
    void setUp() {
        commandA = new CommandImplementation(cA, new Metadata("hello ${planet}", Optional.empty(), true));
        commandB = new CommandImplementation(cB, new Metadata("hello world", Optional.empty(), true));

    }

    private CommandImplementationPriority priority = new CommandImplementationPriority();

    @Test
    void wordSortedOverVariable() {



        var list = Lists.newArrayList(commandA, commandB);
        list.sort(priority);
        assertThat(list).containsExactly(commandB, commandA);
    }
}
