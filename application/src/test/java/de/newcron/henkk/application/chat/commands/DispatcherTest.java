package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class DispatcherTest {


    @Mock
    CommandA commandA;

    @Mock
    CommandB commandB;

    Dispatcher dispatcher;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    InboundMessageArrivedEvent event;


    @BeforeEach
    void setUp() {
        dispatcher = new Dispatcher(new Commands(Arrays.asList(commandA, commandB)));
        Mockito.when(event.getMessage().asString()).thenReturn("b");
    }

    @Test
    void dispatchesToRightTarget() {
        dispatcher.dispatch(event);
        Mockito.verify(commandB).handle(any(), any());
        Mockito.verify(commandA, never()).handle(any(), any());
    }


    @CommandDescription(pattern = "a")
    class CommandA implements Command {

        @Override
        public void handle(ReceivedMessage message, ResponseOptions respond) {
        }
    }

    @CommandDescription(pattern = "b")
    class CommandB implements Command {

        @Override
        public void handle(ReceivedMessage message, ResponseOptions respond) {
        }
    }

}
