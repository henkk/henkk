create table `release_history` (
    id bigint unsigned not null auto_increment,
    project_id bigint unsigned not null,
    release_start_date datetime default CURRENT_TIMESTAMP,
    success bit(1) not null,
    sha varchar(255) not null,
    released_by varchar(255),
    primary key(id),
    foreign key fk_project (project_id) references project(id)
)
