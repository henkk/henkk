alter table project add column current_release_id bigint unsigned default null, add foreign key fk_release_id (current_release_id) references release_history(id);
