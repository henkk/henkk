create table project(
    id bigint unsigned not null auto_increment,
    name varchar(256) not null,
    external_project_id varchar(512) not null,
    project_url varchar(512) not null,
    primary key(id),
    unique key key_external_project_id (external_project_id),
    unique key key_name (name),
    unique key key_project_url (project_url)
);
