package de.newcron.henkk.application.project.persistence;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "project")
public class PersistedProject {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Basic
    private String externalProjectId, name, projectUrl;

    @OneToOne(fetch = FetchType.EAGER)
    private PersistedRelease currentRelease;

}
