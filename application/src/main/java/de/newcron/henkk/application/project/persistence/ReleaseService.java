package de.newcron.henkk.application.project.persistence;

import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release;
import de.newcron.henkk.chat.api.project.ReleasePersistenceService;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@RequiredArgsConstructor
@Component
public class ReleaseService implements ReleasePersistenceService {
    private final ReleaseRepository releaseRepository;

    @Transactional
    public Release createRelease(Project project, Sha revision, UserIdentifier releasedBy) {
        var p = new PersistedRelease();
        p.setProjectId(project.getId().asLong());
        p.setReleaseStartDate(ZonedDateTime.now());
        p.setSha(revision.asString());
        p.setReleasedBy(releasedBy.asString());
        return releaseRepository.save(p);
    }
}
