package de.newcron.henkk.application.chat.routing;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import de.newcron.henkk.chat.api.Message;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static de.newcron.henkk.application.chat.routing.TokenSequence.fromMessage;

@EqualsAndHashCode
@ToString
public class TokenSequencePattern {

    TokenSequencePattern(List<Token> tokens) {
        this.tokens = ImmutableList.copyOf(tokens);
    }

    private final List<Token> tokens;

    List<Token> getTokens() {
        return tokens;
    }


    public static TokenSequencePattern fromPatternSyntax(String pattern) {
        return new TokenSequencePattern(
                fromMessage(pattern)
                        .getTokens()
                        .stream()
                        .map(TokenSequencePattern::asToken)
                        .collect(Collectors.toList()));
    }

    public Result matches(Message message) {
        return matches(TokenSequence.fromMessage(message));
    }

    Result matches(TokenSequence sequence) {
        return new Matcher(tokens).matches(sequence);
    }




    private static final Pattern VARIABLE_PATTERN = Pattern.compile("^\\$\\{([a-zA-Z][a-zA-Z0-9]+)}$");
    private static final Pattern LONG_VARIABLE_PATTERN = Pattern.compile("^\\$\\{([a-zA-Z][a-zA-Z0-9]+)\\.\\.\\}$");

    private static Token asToken(String s) {
        var matcher = LONG_VARIABLE_PATTERN.matcher(s);
        if (matcher.matches()) {
            return new Variable(matcher.group(1), true);
        }
        matcher = VARIABLE_PATTERN.matcher(s);
        if (matcher.matches()) {
            return new Variable(matcher.group(1), false);
        }

        Preconditions.checkArgument(!s.startsWith("$"), "invalid part of pattern string. can't make sense of '" + s + "' if it is a variable, it's badly formatted");


        return new Word(s);
    }



}
