package de.newcron.henkk.application.chat.routing;

import lombok.Data;

@Data
class Word implements Token {
    private final String word;



    @Override
    public boolean accepts(String word) {
        return this.word.equalsIgnoreCase(word);
    }
}
