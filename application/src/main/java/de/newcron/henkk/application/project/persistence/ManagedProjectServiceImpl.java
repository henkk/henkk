package de.newcron.henkk.application.project.persistence;

import de.newcron.henkk.chat.api.project.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@RequiredArgsConstructor
@Component
public class ManagedProjectServiceImpl implements ManagedProjectService {

    private final ProjectRepository projectRepository;

    @Transactional
    @Override
    public void manage(ProjectDescriptor projectDescriptor, String projectName) throws ProjectAlreadyImportedException {
        var projectExisting = projectRepository.findByExternalProjectId(projectDescriptor.getExternalId().asString());
        if(projectExisting.isPresent()) {
            throw new ProjectAlreadyImportedException();
        }
        var projectNameTaken = projectRepository.findByName(projectName);
        if(projectNameTaken.isPresent()) {
            throw new ProjectAlreadyImportedException();
        }

        var p = new PersistedProject();
        p.setExternalProjectId(projectDescriptor.getExternalId().asString());
        p.setProjectUrl(projectDescriptor.getUrl().toString());
        p.setName(projectName);
        projectRepository.save(p);
    }

    @Override
    @Transactional
    public void assignCurrentRelease(long projectId, Release release) {
        var p = projectRepository.findById(projectId).orElseThrow();
        p.setCurrentRelease((PersistedRelease)release);
        projectRepository.save(p);

    }


}
