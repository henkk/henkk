package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.application.chat.routing.Result;

import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Log4j2
public class Dispatcher {

    private final Commands commands;


    public void dispatch(InboundMessageArrivedEvent event) {
        if(event.getMessage().isEmpty()) {
            return;
        }
        for(CommandImplementation c :  commands.getCommandList()) {
            Result match = c.getTokenSequencePattern().matches(event.getMessage());
            if(match.isMatch()) {
                c.getCommand().handle(new ReceivedMessage(event.getMessage(), match.getVariables(), event.getSender()), new ResponseOptions(event));
                return;
            }
        }

        log.error("Received chat message {} but could not find a matching handler. This might indicate a bug in dispatching, as the catch all command should have been able to handle this", event.getMessage().asString());
    }
}
