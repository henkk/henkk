package de.newcron.henkk.application.project.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

interface ProjectRepository extends CrudRepository<PersistedProject, Long> {
    List<PersistedProject> findAll();

    Optional<PersistedProject> findByExternalProjectId(String id);
    Optional<PersistedProject> findByName(String name);
}
