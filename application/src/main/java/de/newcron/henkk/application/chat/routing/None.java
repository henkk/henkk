package de.newcron.henkk.application.chat.routing;

class None implements Token {

    @Override
    public boolean accepts(String word) {
        return false;
    }
}
