package de.newcron.henkk.application.chat;


import de.newcron.henkk.application.chat.commands.Dispatcher;


import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEventListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

@Component
@Log4j2
@RequiredArgsConstructor
class EventProcessor implements InboundMessageArrivedEventListener {

    private final Dispatcher dispatcher;
    private final ExecutorService executorService = Executors.newCachedThreadPool(r -> new Thread(r, "inbound-message-consumer-"+ UUID.randomUUID().toString()));

    @Override
    public void inboundMessageArrived(InboundMessageArrivedEvent event) {
        executorService.execute(() -> dispatcher.dispatch(event));
    }

    @PreDestroy
    void shutdown() throws InterruptedException {
        log.info("shutting down chat event processor");
        executorService.shutdown();
        if(!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
            log.error("system shutting down. thread pool was not terminated within timeout. killing it now");
            executorService.shutdownNow();
        }
    }
}
