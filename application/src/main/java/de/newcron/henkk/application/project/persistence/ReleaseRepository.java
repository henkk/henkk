package de.newcron.henkk.application.project.persistence;

import de.newcron.henkk.chat.api.project.Release;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReleaseRepository extends CrudRepository<PersistedRelease, Long> {
    List<PersistedRelease> findAllByProjectId(long projectId);
}
