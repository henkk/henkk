package de.newcron.henkk.application.ci;


import de.newcron.henkk.application.project.persistence.PersistedRelease;
import de.newcron.henkk.application.project.persistence.ReleaseRepository;
import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.project.ManagedProjectService;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release.ReleaseState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
class PersistedReleaseService {

    private final ReleaseRepository releaseRepository;

    private final ManagedProjectService projectService;


    @Transactional
    PersistedRelease createRelease(Project project, Change change, UserIdentifier releasedBy) {
        return releaseRepository.save(newRelease(project, change, releasedBy));
    }

    private PersistedRelease newRelease(Project project, Change change, UserIdentifier releasedBy) {
        var release = new PersistedRelease();
        release.setReleasedBy(releasedBy.asString());
        release.setReleaseStartDate(ZonedDateTime.now());
        release.setSha(change.getCommit().getSha().asString());
        release.setState(ReleaseState.RUNNING);
        release.setProjectId(project.getId().asLong());
        return release;
    }

    public void persistReleaseOutcome(PersistedRelease createdRelease, ReleaseState state, String message) {
        var refreshedRelease = releaseRepository.findById(createdRelease.getId()).orElseThrow(() -> new IllegalStateException("could not find release " + createdRelease.getId()));

        refreshedRelease.setState(state);
        refreshedRelease.setSuccess(state == ReleaseState.SUCCESS);
        refreshedRelease.setReleaseLog(message);

        releaseRepository.save(refreshedRelease);
        if (state == ReleaseState.SUCCESS) {
            projectService.assignCurrentRelease(refreshedRelease.getProjectId(), refreshedRelease);
        }
    }

    public PersistedRelease failRelease(PersistedRelease createdRelease, String sw) {

        var refreshedRelease = releaseRepository.findById(createdRelease.getId()).orElseThrow(() -> new IllegalStateException("could not find release " + createdRelease.getId()));

        refreshedRelease.setState(ReleaseState.FAILURE);
        refreshedRelease.setSuccess(false);

        refreshedRelease.setReleaseLog(sw);
        return releaseRepository.save(refreshedRelease);
    }
}
