package de.newcron.henkk.application.project.persistence;


import de.newcron.henkk.chat.api.project.ExternalProjectIdentifier;
import de.newcron.henkk.chat.api.project.InternalProjectIdentifier;
import de.newcron.henkk.chat.api.project.ManagedProject;
import de.newcron.henkk.chat.api.project.ManagedProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class ManagedProjectRepositoryImpl implements ManagedProjectRepository {

    private final ProjectRepository projectRepository;

    @Override
    public List<ManagedProject> findAll() {
        return projectRepository.findAll().stream().map(toProject()).toList();
    }

    @Override
    public Optional<ManagedProject> findByProjectName(String name) {
        return projectRepository.findByName(name).map(toProject());
    }

    @Override
    public Optional<ManagedProject> findById(long id) {
        return projectRepository.findById(id).map(toProject());
    }

    private Function<PersistedProject, ManagedProject> toProject() {
        return p ->
            ManagedProject.builder()
                .id(new InternalProjectIdentifier(p.getId()))
                .currentRelease(Optional.ofNullable(p.getCurrentRelease()))
                .name(p.getName())
                .externalIdentifier(new ExternalProjectIdentifier(p.getExternalProjectId()))
                .projectUrl(URI.create(p.getProjectUrl()))
                .build();
    }
}
