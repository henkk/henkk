package de.newcron.henkk.application.chat.routing;

import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class PatternPriority {

    @NonNull
    private final TokenSequencePattern pattern;

    public int score() {
        return pattern.getTokens().stream().mapToInt(this::tokenWeight).sum();

    }

    private int tokenWeight(Token token) {
        if (token instanceof Word) {
            return 100;
        }
        if (token instanceof Variable) {
            return ((Variable) token).isMultipleWordsAllowed() ? 1 : 10;
        }
        return 1;
    }

}
