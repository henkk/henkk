package de.newcron.henkk.application.chat.routing;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import de.newcron.henkk.chat.api.Message;
import lombok.Data;

import java.util.List;

@Data
class TokenSequence {
    private final List<String> tokens;

    TokenSequence(List<String> tokens) {
        this.tokens = ImmutableList.copyOf(tokens);
    }



    static TokenSequence fromMessage(Message message) {
        String sequence = message.asString();
        return fromMessage(sequence);
    }

    static TokenSequence fromMessage(String message) {
        return new TokenSequence(Splitter.on(CharMatcher.whitespace()).omitEmptyStrings().splitToList(message));
    }
}
