package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@CommandDescription(pattern = "help", visible = false)
@RequiredArgsConstructor
class HelpCommand implements Command {

    private final ApplicationContext applicationContext;
    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        var helpVisibleCommands = applicationContext.getBean(Commands.class).getCommandList().stream()
                .map(CommandImplementation::getMetadata)
                .filter(Metadata::isVisible)
                .map(m -> m.getDescription().isPresent() ? String.format("`%s`: %s", m.getPattern(), m.getDescription().get()) : String.format("`%s`", m.getPattern()))
                .collect(Collectors.joining("\n", "Available Commands:\n", ""));

        respond.withText(helpVisibleCommands);
    }
}
