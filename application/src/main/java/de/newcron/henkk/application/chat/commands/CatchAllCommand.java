package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;

// this is not a @Component - it gets added explicitely to ensure it is always the last fallback
@CommandDescription(pattern = "${message..}", visible = false)
class CatchAllCommand implements Command {
    @Override
    public void handle(ReceivedMessage message, ResponseOptions respond) {
        respond.withText("I am sorry, but I don't understand what you're asking me to do. Use 'help' to get a list of commands");
    }

}
