package de.newcron.henkk.application.chat.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;

@Data
class Metadata {
    private final String pattern;
    private final Optional<String> description;
    private final boolean isVisible;
}
