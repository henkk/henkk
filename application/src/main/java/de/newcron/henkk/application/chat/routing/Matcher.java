package de.newcron.henkk.application.chat.routing;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.*;

@Data
class Matcher {
    private final List<Token> pattern;

    Result matches(TokenSequence input) {
        var capturing = new Capturing(pattern, input.getTokens());
        return capturing.capture();
    }


    @RequiredArgsConstructor
    class Capturing {
        @NonNull
        private final List<Token> pattern;
        @NonNull
        private final List<String> tokens;

        private ListIterator<Token> iterator;
        private Token current;
        private Token previous = new None();
        private Map<String, String> variables;
        private boolean complete = false;


        Result capture() {
            if(pattern.isEmpty()) {
                return tokens.isEmpty() ? new Result(true) : new Result(false);
            }

            reset();
            for (String token : tokens) {
                if (complete && canBeAppendedToPrevious()) { // this is to handle cases where a pattern ends with a multi word variable. in that case, all further coming words are to be embedded in the last variable. there's no new current variable anymore.
                    appendToPrevious(token);
                } else if (current.accepts(token)) {
                    memorizeVariableValue(token);
                    if (!next()) {
                        success();
                    }
                } else if (canBeAppendedToPrevious()) {
                    appendToPrevious(token);
                } else if (!complete) {
                    reset();
                }
            }
            return complete ? new Result(true, ImmutableMap.copyOf(variables)) : new Result(false);

        }

        private void appendToPrevious(String token) {
            String variableName = ((Variable) previous).getVariableName();
            String s = variables.get(variableName);
            variables.put(variableName, s + " " + token);
        }


        private boolean canBeAppendedToPrevious() {
            return previous instanceof Variable && ((Variable) previous).isMultipleWordsAllowed();
        }


        private void memorizeVariableValue(String token) {
            if (current instanceof Variable) {
                variables.put(((Variable) current).getVariableName(), token);
            }
        }

        private void success() {
            complete = true;
        }

        private boolean next() {
            previous = current;
            if (!iterator.hasNext()) {
                return false;
            }
            current = iterator.next();
            return true;
        }


        private void reset() {
            iterator = pattern.listIterator();
            current = iterator.next();
            previous = new None();
            variables = Maps.newHashMap();
            complete = false;
        }

    }


}
