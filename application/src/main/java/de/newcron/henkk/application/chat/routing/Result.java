package de.newcron.henkk.application.chat.routing;

import com.google.common.collect.ImmutableMap;
import lombok.Data;

import java.util.Map;

import static java.util.Collections.emptyMap;

@Data
public class Result {
    private final boolean match;
    private final Map<String, String> variables;

    public Result(boolean match) {
        this(match, emptyMap());
    }

    public Result(boolean match, Map<String, String> variables) {
        this.match = match;
        this.variables = ImmutableMap.copyOf(variables);
    }
}
