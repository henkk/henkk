package de.newcron.henkk.application.chat.commands;

import de.newcron.henkk.application.chat.routing.TokenSequencePattern;
import de.newcron.henkk.chat.api.Command;
import lombok.Getter;

@Getter
class CommandImplementation {
    private final Command command;
    private final TokenSequencePattern tokenSequencePattern;
    private final Metadata metadata;

    CommandImplementation(Command command, Metadata metadata) {
        this.command = command;
        this.metadata = metadata;
        tokenSequencePattern = TokenSequencePattern.fromPatternSyntax(metadata.getPattern());
    }
}
