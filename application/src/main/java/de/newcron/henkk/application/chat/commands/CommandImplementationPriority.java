package de.newcron.henkk.application.chat.commands;


import de.newcron.henkk.application.chat.routing.PatternPriority;

class CommandImplementationPriority implements java.util.Comparator<CommandImplementation> {
    @Override
    public int compare(CommandImplementation o1, CommandImplementation o2) {
        var scoreLeft = score(o1);
        var scoreRight = score(o2);
        return scoreRight - scoreLeft;
    }

    private int score(CommandImplementation o2) {
        return new PatternPriority(o2.getTokenSequencePattern()).score();
    }
}
