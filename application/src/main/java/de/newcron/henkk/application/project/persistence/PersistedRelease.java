package de.newcron.henkk.application.project.persistence;

import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.project.Release;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Setter
@Entity
@EqualsAndHashCode
@ToString
@Table(name = "release_history")
public class PersistedRelease implements Release {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Getter
    private long id;

    @Basic
    @Getter
    private long projectId;

    @Basic
    @Getter
    private boolean success;

    @Getter
    private ZonedDateTime releaseStartDate;

    @Basic
    private String sha, releasedBy;

    @Basic
    @Getter
    private String releaseLog;

    @Basic
    @Enumerated(EnumType.STRING)
    @Getter
    private ReleaseState state;


    public Sha getSha() {
        return new Sha(sha);
    }

    @Override
    public UserIdentifier getReleasedBy() {
        return new UserIdentifier(releasedBy);
    }
}
