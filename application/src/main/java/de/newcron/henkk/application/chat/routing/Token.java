package de.newcron.henkk.application.chat.routing;

public interface Token {
    boolean accepts(String word);
}
