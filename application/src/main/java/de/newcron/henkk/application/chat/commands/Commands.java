package de.newcron.henkk.application.chat.commands;

import com.google.common.collect.ImmutableList;

import de.newcron.henkk.chat.api.Command;
import de.newcron.henkk.chat.api.inbound.CommandDescription;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Component
class Commands {

    @Getter
    private final List<CommandImplementation> commandList;



    Commands(@NonNull List<Command> commandList) {
        var commands = commandList.stream().map(this::asImplementation).sorted(new CommandImplementationPriority()).collect(toList());
        commands.add(asImplementation(new CatchAllCommand()));
        this.commandList = ImmutableList.copyOf(commands);
    }



    private CommandImplementation asImplementation(Command command) {
        var annotation = AnnotationUtils.findAnnotation(command.getClass(), CommandDescription.class);
        checkNotNull(annotation, "Command instances must be annotated with " + CommandDescription.class);
        var metadata = new Metadata(annotation.pattern(), ofNullable(emptyToNull(annotation.description())), annotation.visible());
        return new CommandImplementation(command, metadata);
    }

}