package de.newcron.henkk.application.chat.routing;

import lombok.Data;

@Data
class Variable implements Token {
    private final String variableName;
    private final boolean multipleWordsAllowed;

    @Override
    public boolean accepts(String word) {
        return true;
    }
}
