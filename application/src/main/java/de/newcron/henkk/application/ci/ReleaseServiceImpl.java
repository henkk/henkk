package de.newcron.henkk.application.ci;

import de.newcron.henkk.application.project.persistence.PersistedRelease;
import de.newcron.henkk.application.project.persistence.ReleaseRepository;
import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.Change;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService.UpdateListener;
import de.newcron.henkk.chat.api.ci.ReleaseService;
import de.newcron.henkk.chat.api.project.ManagedProjectService;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release;
import de.newcron.henkk.chat.api.project.Release.ReleaseState;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.time.ZonedDateTime;

import static com.google.common.base.Preconditions.checkArgument;

@RequiredArgsConstructor
@Component
@Log4j2
public class ReleaseServiceImpl implements ReleaseService {



    private final ContinuousIntegrationService ciService;

    private final PersistedReleaseService persistedReleaseService;



    @Override
    public Release release(Project project, Change change, UserIdentifier releasedBy, UpdateListener updateListener) {


        var createdRelease = persistedReleaseService.createRelease(project, change, releasedBy);


        try {
            ciService.release(project, change);

            ciService.awaitUpdates(project, change, Duration.ofMinutes(15), updateListener.after((state, message) -> {
                checkArgument(state != ReleaseState.RUNNING);

                persistedReleaseService.persistReleaseOutcome(createdRelease, state, message);
            }));
            return createdRelease;

        } catch (RuntimeException e) {
            log.warn("Release " + createdRelease.getId() + " Failed ", e);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return persistedReleaseService.failRelease(createdRelease, sw.toString());

        }


    }



}
