package de.newcron.henkk.chat.api.outbound.richresponse;

import de.newcron.henkk.chat.api.outbound.RichResponse;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.Builder;

import java.util.List;

@Builder
public class DryRunResponse implements RichResponse {

    public final String projectName;
    public final Sha currentlyDeployedRevision, targetRevision;
    public final List<Commit> commits;


    @Override
    public String asText() {
        var sb = new StringBuilder("... What would hapen if I released ").append(projectName).append("@").append(targetRevision.asString()).append("\n(Currently @").append(currentlyDeployedRevision.asString()).append("\n\nChanges Deployed:");
        for (var c : commits) {
            sb.append("\n* ").append(c.revision).append(": ").append(c.commitMessage).append("/by ").append(c.author);
        }
        return sb.toString();
    }


    @Builder
    public static class Commit {
        public final String author, commitMessage;
        public final Sha revision;
    }

}
