package de.newcron.henkk.chat.api.inbound;


import de.newcron.henkk.chat.api.ChatIdentity;
import de.newcron.henkk.chat.api.Message;
import de.newcron.henkk.chat.api.outbound.OutboundMessage;
import de.newcron.henkk.chat.api.outbound.ResponseChannel;
import lombok.Data;
import lombok.NonNull;

import java.util.function.Consumer;

@Data
public class InboundMessageArrivedEvent {
    @NonNull private final ChatIdentity sender;
    @NonNull private final Message message;
    @NonNull private final ResponseChannel responseChannel;

    public void respondWith(Consumer<OutboundMessage.OutboundMessageBuilder> responseConfigurer) {
        var builder = OutboundMessage.builder();
        responseConfigurer.accept(builder);
        responseChannel.reply(this, builder.build());
    }
}
