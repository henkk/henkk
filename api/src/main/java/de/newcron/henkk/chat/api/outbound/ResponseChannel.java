package de.newcron.henkk.chat.api.outbound;

import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;

public interface ResponseChannel {
    void reply(InboundMessageArrivedEvent inboundMessageArrivedEvent, OutboundMessage reply);
}
