package de.newcron.henkk.chat.api.outbound;


public interface RichResponse {
    String asText();

    default ResponseType getResponseType() {
        return ResponseType.PUBLIC;
    }
}
