package de.newcron.henkk.chat.api.inbound;

import de.newcron.henkk.chat.api.ChatIdentity;
import de.newcron.henkk.chat.api.Message;
import lombok.*;

import java.util.Map;

@Data
@RequiredArgsConstructor
public class ReceivedMessage {

    @NonNull
    @Getter
    private final Message message;

    @NonNull
    private final Map<String, String> variables;

    @NonNull
    private final ChatIdentity sender;

}
