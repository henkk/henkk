package de.newcron.henkk.chat.api.project;

import lombok.NonNull;

import java.net.URI;
import java.util.Optional;

public interface Project {

    @NonNull
    URI getProjectUrl();
    @NonNull
    String getName();


    @NonNull
    ExternalProjectIdentifier getExternalIdentifier();

    @NonNull
    InternalProjectIdentifier getId();

    @NonNull Optional<Release> getCurrentRelease();
}
