package de.newcron.henkk.chat.api.project;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode
public class ExternalProjectIdentifier {
    private final String id;

    public String asString() {
        return id;
    }
}
