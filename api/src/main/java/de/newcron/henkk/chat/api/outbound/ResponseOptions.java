package de.newcron.henkk.chat.api.outbound;

import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Data
public class ResponseOptions {

    @NonNull
    private final InboundMessageArrivedEvent event;

    private void with(Consumer<OutboundMessage.OutboundMessageBuilder> responseConfigurer) {
        var builder = OutboundMessage.builder();
        responseConfigurer.accept(builder);
        event.getResponseChannel().reply(event, builder.build());
    }

    public void withText(String s) {
        with(() -> s);
    }

    public void with(RichResponse response) {
        event.getResponseChannel().reply(event, new OutboundMessage(response));
    }
}
