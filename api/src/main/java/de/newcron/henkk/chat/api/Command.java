package de.newcron.henkk.chat.api;

import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.inbound.ReceivedMessage;
import de.newcron.henkk.chat.api.outbound.ResponseChannel;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;

public interface Command {

    void handle(ReceivedMessage message, ResponseOptions respond);
}
