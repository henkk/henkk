package de.newcron.henkk.chat.api.ci;

public interface Change {
    Commit getCommit();

    boolean isReadyForDeployment();

    PipelineState getPipelineState();

    enum PipelineState {
        PENDING, RUNNING, GREEN, RED, ABORTED
    }
}
