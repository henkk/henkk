package de.newcron.henkk.chat.api.project;

import de.newcron.henkk.chat.api.ChatIdentity;
import lombok.NonNull;

import java.net.URI;
import java.util.List;


public interface ProjectDescriptor {
    @NonNull
    String getName();

    @NonNull
    URI getUrl();

    @NonNull
    ExternalProjectIdentifier getExternalId();

}
