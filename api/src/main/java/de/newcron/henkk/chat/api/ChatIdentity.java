package de.newcron.henkk.chat.api;

import lombok.*;

@Data
public class ChatIdentity {
    @NonNull
    private final DisplayName nickname;
    @NonNull
    private final UserIdentifier uniqueUserIdentifier;


}
