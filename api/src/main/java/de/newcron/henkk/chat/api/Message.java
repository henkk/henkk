package de.newcron.henkk.chat.api;

import de.newcron.henkk.chat.api.ChatIdentity;
import de.newcron.henkk.chat.api.Message;
import lombok.*;

@EqualsAndHashCode
@ToString
@Builder
public class Message {
    @NonNull
    private final String message;

    public String asString() {
        return message;
    }

    public boolean isEmpty() {
        return message.isBlank();
    }

}
