package de.newcron.henkk.chat.api.ci;

import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.ci.ContinuousIntegrationService.UpdateListener;
import de.newcron.henkk.chat.api.outbound.ResponseOptions;
import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release;
import de.newcron.henkk.chat.api.project.Sha;

public interface ReleaseService {
    Release release(Project project, Change change, UserIdentifier releasedBy, UpdateListener updateListener);
}
