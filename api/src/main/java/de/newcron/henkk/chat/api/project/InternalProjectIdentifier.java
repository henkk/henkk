package de.newcron.henkk.chat.api.project;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode
public class InternalProjectIdentifier {
    private final long id;
    public Long asLong() {
        return id;
    }
}
