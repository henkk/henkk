package de.newcron.henkk.chat.api.project;

public interface ManagedProjectService {


    class ProjectAlreadyImportedException extends Exception {
    }

    class ProjectNameAlreadyTakenException extends Exception {
    }

    void manage(ProjectDescriptor projectDescriptor, String projectName) throws ProjectAlreadyImportedException, ProjectNameAlreadyTakenException;

    void assignCurrentRelease(long projectId, Release release);

}
