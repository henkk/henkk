package de.newcron.henkk.chat.api.outbound;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundMessage {

    private final RichResponse response;


}
