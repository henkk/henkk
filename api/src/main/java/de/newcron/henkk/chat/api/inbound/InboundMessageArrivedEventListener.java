package de.newcron.henkk.chat.api.inbound;

public interface InboundMessageArrivedEventListener {

    void inboundMessageArrived(InboundMessageArrivedEvent event);
}
