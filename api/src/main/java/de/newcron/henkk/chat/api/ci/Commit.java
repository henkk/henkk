package de.newcron.henkk.chat.api.ci;

import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.project.Sha;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class Commit {

    private final Sha sha;

    private final UserIdentifier committer;

    private final ZonedDateTime timestamp;

    private final boolean onDefaultBranch;

    private final String message;

}
