package de.newcron.henkk.chat.api.project;

import java.util.List;
import java.util.Optional;

public interface ManagedProjectRepository {
    List<ManagedProject> findAll();

    Optional<ManagedProject> findByProjectName(String name);

    Optional<ManagedProject> findById(long id);
}
