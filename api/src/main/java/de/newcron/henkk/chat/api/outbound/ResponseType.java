package de.newcron.henkk.chat.api.outbound;

public enum ResponseType {
    PUBLIC, EPHEMERAL, PRIVATE_MESSAGE
}
