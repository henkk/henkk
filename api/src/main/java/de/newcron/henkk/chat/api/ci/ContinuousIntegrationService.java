package de.newcron.henkk.chat.api.ci;

import de.newcron.henkk.chat.api.project.Project;
import de.newcron.henkk.chat.api.project.Release.ReleaseState;

import java.time.Duration;

public interface ContinuousIntegrationService {

    void release(Project project, Change change);

    void awaitUpdates(Project project, Change change, Duration timeout, UpdateListener updateListener);


    interface UpdateListener {
        void onUpdate(ReleaseState state, String message);

        default UpdateListener andThen(UpdateListener second) {
            var me = this;
            return (state, message) -> {
                me.onUpdate(state, message);
                second.onUpdate(state, message);
            };
        }
        default UpdateListener after(UpdateListener before) {
            var me = this;
            return (state, message) -> {
                before.onUpdate(state, message);
                me.onUpdate(state, message);
            };
        }
    }
}
