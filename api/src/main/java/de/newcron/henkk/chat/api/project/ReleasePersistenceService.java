package de.newcron.henkk.chat.api.project;

import de.newcron.henkk.chat.api.UserIdentifier;

public interface ReleasePersistenceService {
    Release createRelease(Project project, Sha sha, UserIdentifier releasedBy);
}
