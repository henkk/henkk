package de.newcron.henkk.chat.api.inbound;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CommandDescription {
    String pattern();
    String description() default "";
    String group() default "";
    boolean visible() default true;
}
