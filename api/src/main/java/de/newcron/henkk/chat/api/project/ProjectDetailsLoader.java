package de.newcron.henkk.chat.api.project;

import java.util.Optional;

public interface ProjectDetailsLoader {
    Optional<ProjectDescriptor> load(ExternalProjectIdentifier externalProjectIdentifier);
}
