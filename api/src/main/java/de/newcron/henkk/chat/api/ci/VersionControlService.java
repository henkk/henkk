package de.newcron.henkk.chat.api.ci;

import de.newcron.henkk.chat.api.project.ManagedProject;
import de.newcron.henkk.chat.api.project.Sha;

public interface VersionControlService {
    Change getChangeForCommit(ManagedProject project, Sha sha);

    ChangeDiff getDiff(ManagedProject managedProject, Sha sha, Sha revision);
}
