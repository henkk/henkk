package de.newcron.henkk.chat.api.project;

import de.newcron.henkk.chat.api.UserIdentifier;

import java.time.ZonedDateTime;

public interface Release {

    long getId();

    ZonedDateTime getReleaseStartDate();

    boolean isSuccess();

    Sha getSha();

    String getReleaseLog();

    UserIdentifier getReleasedBy();

    ReleaseState getState();

    enum ReleaseState {
        RUNNING, SUCCESS, FAILURE, ABORTED;
    }
}
