package de.newcron.henkk.chat.api.project;


import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NonNull;

import java.net.URI;
import java.util.EmptyStackException;
import java.util.Optional;

@Data
@Builder
public class ManagedProject implements Project {
    @NonNull
    private final URI projectUrl;
    @NonNull
    private final String name;
    @NonNull
    private final ExternalProjectIdentifier externalIdentifier;

    @NonNull
    private final InternalProjectIdentifier id;

    @NonNull
    @Default
    private final Optional<Release> currentRelease = Optional.empty();

}
