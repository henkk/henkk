package de.newcron.henkk.chat.api.ci;

import lombok.Data;

import java.util.List;

@Data
public class ChangeDiff {

    private final Change change;

    private final List<Commit> previousCommits;
}
