package de.newcron.henkk.chat.api;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
/**
 * Protocol Specific unique identifier of a user. Can be a user id or the IM name
 */
public class UserIdentifier {
    @NonNull
    private final String identifier;

    public String asString() {
        return identifier;
    }
}
