package de.newcron.henkk.chat.api.ci;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@RequiredArgsConstructor
public class DefaultChange implements Change {
    private final Commit commit;
    private final boolean isReadyForDeployment;
    private final PipelineState pipelineState;

}
