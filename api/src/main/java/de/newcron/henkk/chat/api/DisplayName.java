package de.newcron.henkk.chat.api;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
/**
 * Human Friendly and Readable name of a user. This is not necessarily the real id of that user.
 */
public class DisplayName {
    @NonNull
    private final String nickname;

    public String asString() {
        return nickname;
    }
}
