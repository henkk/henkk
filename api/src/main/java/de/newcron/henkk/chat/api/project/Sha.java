package de.newcron.henkk.chat.api.project;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode
public class Sha {
    @NonNull
    private String asString;

    public String asString() {
        return asString;
    }
}
