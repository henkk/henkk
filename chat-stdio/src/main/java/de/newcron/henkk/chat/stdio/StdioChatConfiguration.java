package de.newcron.henkk.chat.stdio;


import de.newcron.henkk.chat.api.ChatClient;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEventListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "henkk.chat", havingValue = "stdio")
@ComponentScan(basePackageClasses = StdioChatConfiguration.class)
public class StdioChatConfiguration {

    @Bean
    public ChatClient connect(InboundMessageArrivedEventListener listener) {
        var client = new StdioChatClient(listener);
        client.connect();

        return client;

    }
}
