package de.newcron.henkk.chat.stdio;

import de.newcron.henkk.chat.api.ChatClient;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEventListener;
import de.newcron.henkk.chat.api.inbound.InboundMessageArrivedEvent;
import de.newcron.henkk.chat.api.outbound.OutboundMessage;
import de.newcron.henkk.chat.api.ChatIdentity;
import de.newcron.henkk.chat.api.Message;
import de.newcron.henkk.chat.api.UserIdentifier;
import de.newcron.henkk.chat.api.DisplayName;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.logging.log4j.LogManager.getLogger;

@RequiredArgsConstructor
class StdioChatClient implements ChatClient {

    private static final Logger LOG = getLogger(StdioChatClient.class);
    private static final Logger RESPONSE_LOGGER = getLogger("stdin_chat_client_response_logger");

    private volatile Thread listenerThread;
    private final InboundMessageArrivedEventListener listener;

    private Thread listenerThread(InboundMessageArrivedEventListener listener) {
        var t = new Thread(new ListenerRunnable(listener), "stdin-chat-client-thread");
        t.setDaemon(true);
        t.start();
        return t;
    }


    class ListenerRunnable implements Runnable {

        private final InboundMessageArrivedEventListener listener;

        ListenerRunnable(InboundMessageArrivedEventListener listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            var input = new Scanner(System.in);
            while (running.get()) {
                if (!input.hasNextLine()) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        LOG.warn("receiving messages was interrupted. shutting down", e);
                        close();
                        return;
                    }
                    continue;
                }
                String currentUser = System.getProperty("user.name");
                listener.inboundMessageArrived(
                    new InboundMessageArrivedEvent(
                        new ChatIdentity(new DisplayName(currentUser), new UserIdentifier(currentUser)),
                        Message.builder().message(input.nextLine()).build(), this::sendResponse));

            }
        }

        private void sendResponse(InboundMessageArrivedEvent inboundMessageArrivedEvent, OutboundMessage outboundMessage) {
            RESPONSE_LOGGER.info(outboundMessage.getResponse().asText());
        }

    }

    private final AtomicBoolean running = new AtomicBoolean(false);

    void connect() {

        if (listenerThread != null) {
            throw new IllegalStateException("chat client is already running");
        }
        running.set(true);
        listenerThread = listenerThread(listener);

    }

    @Override
    public synchronized void close() {
        running.set(false);
        listenerThread = null;
    }
}
